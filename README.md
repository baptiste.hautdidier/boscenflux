![Image](Logo-BoscEnFlux.png)

*Analyser les trajectoires des espaces forestiers aquitains par leurs marges sociales et écologiques*, un projet de recherche soutenu par la région Nouvelle Aquitaine (conventions 2017 - LR40111 - 00013262/3/4)
# Publications du programme

### Articles dans des revues à comité de lecture


###### **[Ginter & Hautdidier, 2022a]** <br> Ginter Z., Hautdidier B. « The ‘gift of the new world’: Retelling the trajectories of black Locust in France » in *Journal of Historical Geography*, volume 78, octobre 2022, p. 115-125. [[➔DOI]](https://dx.doi.org/10.1016/j.jhg.2022.06.001) [[➔HAL]](https://hal.science/hal-03808744) [[➔ResearchGate]](https://www.researchgate.net/publication/363202846_The_&#39;gift_of_the_new_world&#39;_Retelling_the_trajectories_of_black_Locust_in_France)

###### **[Krasnodębski, 2019]** <br> Krasnodębski M. « Challenging the Pine: Epistemic Underpinnings of Techno-Environmental Inertia » in *Journal for the History of Environment and Society*, volume 4, 2019, p. 41-69. [[➔DOI]](https://www.brepolsonline.net/doi/pdf/10.1484/J.JHES.5.120675) [[➔ResearchGate]](https://www.researchgate.net/publication/343669543_Challenging_the_Pine_Epistemic_Underpinnings_of_Techno-Environmental_Inertia)

###### **[Krasnodębski, 2021]** <br> Krasnodębski M. « Upscaling Forest Waste: The French Quest for Fuel Autarky after World War I » in *Technology and culture*, vol. 62 n° 1, 2021, p. 105-127. [[➔DOI]](https://doi.org/10.1353/tech.2021.0004) [[➔ResearchGate]](https://www.researchgate.net/publication/349875820_Upscaling_Forest_Waste_The_French_Quest_for_Fuel_Autarky_after_World_War_I)

###### **[Krasnodębski, 2022]** <br> Krasnodębski M. « The Social Construction of Pine Forest Wastes in Southwestern France During the Nineteenth and Twentieth Centuries » in *Environment and History*. volume 28, n° 1, février 2022, p. 155-183. [[➔DOI]](https://doi.org/10.3197/096734019X15740974883843) [[➔ResearchGate]](https://www.researchgate.net/publication/337623934_The_Social_Construction_of_Pine_Forest_Wastes_in_Southwestern_France_During_the_Nineteenth_and_Twentieth_Centuries)

###### **[Nageleisen et al., 2020]** <br> Nageleisen S., Hautdidier B., Couderchet L., Ginter Z. « Ce que les transitions forestières font à l’expérience paysagère. Considérations géographiques à partir du cas des robineraies du Sud-Gironde » in *Projets de paysage*, volume 22, 2020, [[➔DOI]](https://doi.org/10.4000/paysage.8637) [[➔HAL]](https://hal.inrae.fr/hal-03177531) [[➔ResearchGate]](https://www.researchgate.net/publication/343158718_Ce_que_les_transitions_forestieres_font_a_l&#39;experience_paysagere_Considerations_geographiques_a_partir_du_cas_des_robineraies_du_Sud-Gironde_The_Effects_of_Forest_Transitions_on_the_Landscape_Experien)

----
### Articles dans des revues sans comité de lecture

###### **[Hautdidier et al., 2023]** <br> Hautdidier B., Banos V., Krasnodębski M., Ginter Z., Couderchet L., Bouisset Ch., Lemoigne N., Nageleisen S., Porté A., Kull Ch. « Au-delà du face-à-face entre une forêt industrialisée et ses 'marges sauvages' : dynamiques d’innovation et processus de requalification des espaces forestiers en Aquitaine » in *revue POUR*, n° 246, p. 55-62. [[➔DOI]](https://doi.org/10.3917/pour.246.0055) [[➔ResearchGate]](https://www.researchgate.net/publication/374128386_Au-dela_du_face-a-face_entre_une_foret_industrialisee_et_ses_marges_sauvages_dynamiques_d%27innovation_et_processus_de_requalification_des_espaces_forestiers_en_Aquitaine/references)



----
### Thèses et mémoires universitaires 

###### **[Bastard, 2019]** <br> Bastard M. *Le châtaignier en Périgord ou les trajectoires incertaines d'une ressource territoriale : un regard généalogique et exploratoire entre nature et culture*, mémoire de Master 2 GAED (Géographie, aménagement, environnement et développement, parcours Paysage, environnement, participation, société), Agro Campus Ouest, Université d'Angers, 2019. [[➔Dumas]](https://dumas.ccsd.cnrs.fr/dumas-02433616)

###### **[Ginter, 2023]** <br> Ginter Z. *Imaginaires arborés, attentes contrariées et pratiques de la marge. Géographie sociale du Robina pseudoacacia dans le Sud-Ouest de la France*, thèse de doctorat en géographie, Université Bordeaux-Montaigne, soutenue le 5 janvier 2023. [[➔theses.fr]](https://www.theses.fr/s189039)

###### **[Macret, 2019]** <br> Macret, A. *La mobilisation de la biomasse sur le massif Adour-Pyrénées. Dans quelles mesures l’apparition et le développement du marché du bois énergie a favorisé un processus de structuration territoriale de la filière forêt- bois dans les Pyrénées-Atlantiques dans un objectif de mobilisation de la ressource bois ?*, mémoire de Master 1 GTDL (gestion des Territoires et Développement Local), Université Lumière Lyon 2, 2019.

###### **[Sénéca-Ferreira, 2018]** <br> Sénéca-Ferreira, J. *Mémoire orale et traces paysagères des usages paysans du Robinier en Gironde* [titre de travail], mémoire de Master 2 DTOQP (Développement des Territoires, Origine et Qualité des Produits), Université Bordeaux-Montaigne, 2018.

----
### Communications à des colloques internationaux

###### **[Ginter et al., 2018]** <br> Ginter Z., Hautdidier B., Sénéca-Ferreira J. « Stuck behind vineyard stakes? Uses and narratives around the black Locust forests of a winemaking region in southwestern France. » Communication orale au colloque *Non-native tree species for European forests* (COST Action FP1403 NNEXT *International Conference*), 12-14 Sept. 2018, Vienne, Autriche [[➔COST]](https://www.cost.eu/actions/FP1403/#downloads) [[➔HAL]](https://hal.inrae.fr/hal-02610015)

###### **[Ginter & Hautdidier, 2018]** <br> Ginter Z., Hautdidier B. « Mapping black Locust stands with Sentinel-2 and Venµs time series images. A test in Médoc, France » Poster au colloque *Non-native tree species for European forests* (COST Action FP1403 NNEXT *International Conference*), 12-14 Sept. 2018, Vienne, Autriche [[➔COST]](https://www.cost.eu/actions/FP1403/#downloads) [[➔HAL]](https://hal.inrae.fr/hal-04160611)

###### **[Hautdidier et al., 2020]** <br> Hautdidier B., Ginter Z., Banos V. « On the merits of examining regional forest transformations in South-western France with a ‘plantationocene’ lens. » Communication orale (visio) au colloque *POLLEN 2020 Contested Natures: Power, Possibility, Prefiguration.*, Sep 2020, Brighton. [[➔HAL]](https://hal.inrae.fr/hal-03222588)

###### **[Ginter & Hautdidier, 2022b]** <br> Ginter Z., Hautdidier B. « Upbeat plants in times of forest crisis? On the material and discursive trajectories of black Locust in France. » Communication au *colloque UGI Paris 2022 'le temps des géographes'*, session *Making time for plants; marking the temporalities, rythms and durations of vegetal life in times of crisis*. 21 juillet, Paris [[➔Site UGI]](https://www.ugiparis2022.org/#collapse69017)

----
### Autres communications

###### **[Hautdidier, 2020]** <br> Hautdidier B. « Aquitaine, une étymologie bien fondée (?) Évolutions et permanences de la conception des relations entre forêt et eau dans le massif Landais. » Communication invitée au *Colloque "Klaus 2020": La forêt et l'eau*, Jan 2020, Sabres, PNR des Landes de Gascogne. [[➔HAL]](https://hal.inrae.fr/hal-02610339)

###### **[Hautdidier, 2019]** <br> Hautdidier B. « Les espaces forestiers comme suports de luttes sociales » Intervention à l'Université Populaire de Bordeaux, chaire *écologie(s) critique(s)*, 17 janvier 2019. [[➔Youtube (audio)]](https://youtu.be/9nASzNLqQO0)

###### **[ALGA, 2023]** <br> ALGA médiation, avec le collectif BoscEnFlux « Acacias en Sauternais. La forêt en mouvement », Film de médiation scientifique, 2023. [[➔Vimeo ]](https://vimeo.com/844969528)


----



